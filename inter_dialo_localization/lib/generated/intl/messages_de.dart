// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a de locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'de';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static _notInlinedMessages(_) => <String, Function> {
    "alertDialogButtonName" : MessageLookupByLibrary.simpleMessage("Alarmdialog"),
    "appBarTitle" : MessageLookupByLibrary.simpleMessage("Intl, Lokalisierung und Dialoge"),
    "assignmentSelectViewText" : MessageLookupByLibrary.simpleMessage("SWählen Sie die Zuordnung aus, die Sie anzeigen möchten."),
    "assignmentSelectionText" : MessageLookupByLibrary.simpleMessage("Zuweisungsauswahl"),
    "cupertinoDialogButtonName" : MessageLookupByLibrary.simpleMessage("Cupertino Dialog"),
    "highHeatDismissButton" : MessageLookupByLibrary.simpleMessage("Entlassen"),
    "highHeatNotification" : MessageLookupByLibrary.simpleMessage("In San Diego werden für die nächsten Tage hohe Temperaturen erwartet."),
    "highHeatTitle" : MessageLookupByLibrary.simpleMessage("Wärmehinweis Warnung!"),
    "highTrafficDismissButton" : MessageLookupByLibrary.simpleMessage("Entlassen"),
    "highTrafficNotification" : MessageLookupByLibrary.simpleMessage("Starker Verkehr in Ihrer Nähe, erwarten Sie Verkehrsverlangsamungen. Planen Sie entsprechend."),
    "highTrafficTitle" : MessageLookupByLibrary.simpleMessage("Benachrichtigung über hohen Datenverkehr"),
    "lab1Text" : MessageLookupByLibrary.simpleMessage("Labor 1"),
    "lab2Text" : MessageLookupByLibrary.simpleMessage("Labor 2"),
    "simpleDialogButtonName" : MessageLookupByLibrary.simpleMessage("Einfacher Dialog")
  };
}
