// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a pt_BR locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'pt_BR';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static _notInlinedMessages(_) => <String, Function> {
    "alertDialogButtonName" : MessageLookupByLibrary.simpleMessage("Diálogo de Alerta"),
    "appBarTitle" : MessageLookupByLibrary.simpleMessage("Intl, localilização e diálogos"),
    "assignmentSelectViewText" : MessageLookupByLibrary.simpleMessage("Selecione a tarefa que deseja visualizar."),
    "assignmentSelectionText" : MessageLookupByLibrary.simpleMessage("Seleção de Atribuição"),
    "cupertinoDialogButtonName" : MessageLookupByLibrary.simpleMessage("Diálogo Cupertino"),
    "highHeatDismissButton" : MessageLookupByLibrary.simpleMessage("Dispensar"),
    "highHeatNotification" : MessageLookupByLibrary.simpleMessage("Esperam-se altas temperaturas em San Diego nos próximos dias. Fique fresco e bem hidratado."),
    "highHeatTitle" : MessageLookupByLibrary.simpleMessage("Aviso de calor!"),
    "highTrafficDismissButton" : MessageLookupByLibrary.simpleMessage("Dispensar"),
    "highTrafficNotification" : MessageLookupByLibrary.simpleMessage("Tráfego intenso na sua área, espere lentidão no tráfego. Planeje de acordo."),
    "highTrafficTitle" : MessageLookupByLibrary.simpleMessage("Notificação de tráfego intenso"),
    "lab1Text" : MessageLookupByLibrary.simpleMessage("Laboratório 1"),
    "lab2Text" : MessageLookupByLibrary.simpleMessage("Laboratório 2"),
    "simpleDialogButtonName" : MessageLookupByLibrary.simpleMessage("Diálogo Simples")
  };
}
