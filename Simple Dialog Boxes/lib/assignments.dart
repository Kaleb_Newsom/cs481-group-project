import 'package:flutter/material.dart';

class assignments extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Assignments'),
        actions: <Widget>[
          IconButton(
              icon: Icon(Icons.search),
              onPressed: () {}
          ),
        ],
      ),
      body: Container(
        child: SimpleDialog(
          title: const Text('Assignment Selection'),
          children: <Widget>[
            const Text('      Select the assignment you wish to view.'),
            SizedBox(height: 10,),
            SimpleDialogOption(
              onPressed: () {Navigator.pushReplacementNamed(context, '/lab1');}, //Navigator.pop(context, Department.treasury); },
              child: const Text('Lab1'),
            ),
            SimpleDialogOption(
              onPressed: () {Navigator.pushReplacementNamed(context, '/lab2');}, //Navigator.pop(context, Department.state); },
              child: const Text('Lab2'),
            ),
          ],
        ),
      ),
    );
  }
}
