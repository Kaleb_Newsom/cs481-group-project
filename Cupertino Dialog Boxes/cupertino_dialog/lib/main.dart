import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';


void main()=>
    runApp(MaterialApp(
      home: Dialogs(),
    ));


class Dialogs extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: CupertinoAlertDialog(
          title: Text('High Traffic notification'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text('High traffic in your area, expect traffic slowdowns. Plan accordingly.'),
                SizedBox(height: 10,),
                //Text('Sep 6, 7:24 AM'),
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('Dismiss'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        ),
      ),
    );
  }
}


