import 'package:flutter/material.dart';


void main()=>
  runApp(MaterialApp(
    home: Dialogs(),
  ));


class Dialogs extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text('High Traffic notification'),
      content: SingleChildScrollView(
        child: ListBody(
          children: <Widget>[
            Text('High traffic in your area, expect traffic slowdowns. Plan accordingly.'),
            SizedBox(height: 10,),
            //Text('Sep 6, 7:24 AM'),
          ],
        ),
      ),
      actions: <Widget>[
        FlatButton(
          child: Text('Dismiss'),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
      ],
    );
  }
}







      /*
      Scaffold(
      body: Container(
        child: Center(
            child: RaisedButton(
              color: Colors.blue[200],
              child: Text('Show dialog',
                style: TextStyle(
                color: Colors.purple[700],
              ),
              ),
              onPressed: () {
                _showMyDialog(context);
              },
            )
        ),
      ),
    );
    */

  //}

  /*
    Future<void> _showMyDialog(context) async {
      return showDialog<void>(
        context: context,
        barrierDismissible: false, // user must tap button!
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('Tornado Warning'),
            content: SingleChildScrollView(
              child: ListBody(
                children: <Widget>[
                  Text('Tornado weather expected.'),
                  Text('Please find a safe place to seek shelter!'),
                ],
              ),
            ),
            actions: <Widget>[
              FlatButton(
                child: Text('Dismiss'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        },
      );
    }
*/
//}
