import 'package:flutter/material.dart';
import 'generated/l10n.dart';

class alert extends StatefulWidget {
  @override
  _alertState createState() => _alertState();
}

class _alertState extends State<alert> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: AlertDialog(
        title: Text(S.of(context).highHeatTitle),
        content: SingleChildScrollView(
          child: ListBody(
            children: <Widget>[
              Text(S.of(context).highHeatNotification),
              SizedBox(height: 10,),
              //Text('Sep 6, 7:24 AM'),
            ],
          ),
        ),
        actions: <Widget>[
          FlatButton(
            child: Text(S.of(context).highHeatDismissButton),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      ),
    );
  }
}
