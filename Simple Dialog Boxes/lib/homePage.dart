import 'package:flutter/material.dart';
import 'assignments.dart';

class Dialogs extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('CS481 Mobile Programming'),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.search),
            onPressed: () {}
          ),
        ],
      ),
        drawer: Drawer(
          child: ListView(
            padding: EdgeInsets.zero,
            children: <Widget>[
              Container(
                height: 80,
                child: DrawerHeader(
                  child: Text('CS 481 Mobile Programming'),
                  decoration: BoxDecoration(
                    color: Colors.blue,
                  ),
                  ),
              ),
              ListTile(title: Text('Introduction'),
                  onTap: (){}
              ),
              ListTile(
                title: Text('Assignments'),
                onTap: (){
                  Navigator.pushReplacementNamed(context, '/assignments');
                },
              ),
              ListTile(title: Text('Grades'),
                onTap: (){}
                ),
            ],
          ),
        ),

    );
  }
}
