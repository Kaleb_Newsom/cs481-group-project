import 'package:flutter/material.dart';
import 'generated/l10n.dart';
import 'package:flutter/cupertino.dart';

class cupertino extends StatefulWidget {
  @override
  _cupertinoState createState() => _cupertinoState();
}

class _cupertinoState extends State<cupertino> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CupertinoAlertDialog(
        title: Text(S.of(context).highTrafficTitle, textAlign: TextAlign.left,),
        content: SingleChildScrollView(
          child: ListBody(
            children: <Widget>[
              SizedBox(height: 10,),
              Text(S.of(context).highTrafficNotification, textAlign: TextAlign.left,),
              SizedBox(height: 10,),
              //Text('Sep 6, 7:24 AM'),
            ],
          ),
        ),
        actions: <Widget>[
          FlatButton(
            child: Text(S.of(context).highTrafficDismissButton),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      ),
    );
  }
}
