import 'package:flutter/material.dart';
import 'generated/l10n.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'homeScreen.dart';
import 'simple.dart';
import 'cupertino.dart';
import 'alert.dart';


void main()=>
    runApp(MaterialApp(
      routes: {
        '/simple': (context) => simple(),
        '/cupertino': (context) => cupertino(),
        '/alert': (context) => alert(),
      },
      home: homeScreen(),
      localizationsDelegates: [
        S.delegate,
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      supportedLocales: S.delegate.supportedLocales,
    ));

