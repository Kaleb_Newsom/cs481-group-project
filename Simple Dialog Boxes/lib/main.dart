import 'package:flutter/material.dart';
import 'homePage.dart';
import 'lab1.dart';
import 'lab2.dart';
import 'assignments.dart';


void main() {
  runApp(MaterialApp(
      routes: {
        '/': (context) => Dialogs(),   // home screen
        '/lab1' : (context) => lab1(),
        '/lab2' : (context) => lab2(),
        '/assignments' : (context) => assignments(),
      }
  ));
}
