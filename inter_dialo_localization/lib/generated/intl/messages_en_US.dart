// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a en_US locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'en_US';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static _notInlinedMessages(_) => <String, Function> {
    "alertDialogButtonName" : MessageLookupByLibrary.simpleMessage("Alert Dialog"),
    "appBarTitle" : MessageLookupByLibrary.simpleMessage("Intl, Localilization, and Dialogs"),
    "assignmentSelectViewText" : MessageLookupByLibrary.simpleMessage("Select the assignment you wish to view."),
    "assignmentSelectionText" : MessageLookupByLibrary.simpleMessage("Assignment Selection"),
    "cupertinoDialogButtonName" : MessageLookupByLibrary.simpleMessage("Cupertino Dialog"),
    "highHeatDismissButton" : MessageLookupByLibrary.simpleMessage("Dismiss"),
    "highHeatNotification" : MessageLookupByLibrary.simpleMessage("High temperatures expected in San Diego for the next several days. Stay cool and well hydrated."),
    "highHeatTitle" : MessageLookupByLibrary.simpleMessage("Heat Advisory Warning!"),
    "highTrafficDismissButton" : MessageLookupByLibrary.simpleMessage("Dismiss"),
    "highTrafficNotification" : MessageLookupByLibrary.simpleMessage("High traffic in your area, expect traffic slowdowns. Plan accordingly."),
    "highTrafficTitle" : MessageLookupByLibrary.simpleMessage("High Traffic Notification"),
    "lab1Text" : MessageLookupByLibrary.simpleMessage("Lab 1"),
    "lab2Text" : MessageLookupByLibrary.simpleMessage("Lab 2"),
    "simpleDialogButtonName" : MessageLookupByLibrary.simpleMessage("Simple Dialog")
  };
}
