import 'package:flutter/material.dart';


class lab2 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blue[800],
        centerTitle: true,
        title: Text('Lab2 assignment'),
        ),
      body: Container(
        margin: EdgeInsets.all(5),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              SizedBox(
                width: 50,
                height: 30,
                child: FlatButton(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20),
                  ),
                  padding: EdgeInsets.all(8.0),
                  color: Colors.blue[100],
                  //icon: Icon(Icons.navigate_before),
                  //iconSize: 30,
                  child: Text('Back'),
                  //alignment: Alignment.center,
                  onPressed: () => {
                    Navigator.pushReplacementNamed(context, '/assignments')
                  },
                ),
              ),
            ],
          )
      ),

    );
  }
}
