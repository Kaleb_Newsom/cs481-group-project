import 'package:flutter/material.dart';
import 'generated/l10n.dart';

class simple extends StatefulWidget {
  @override
  _simpleState createState() => _simpleState();
}

class _simpleState extends State<simple> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SimpleDialog(
        title: Text(S.of(context).assignmentSelectionText, textAlign: TextAlign.center),
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.fromLTRB(14.0, 0.0, 0.0, 0.0),
            child: Text(S.of(context).assignmentSelectViewText, textAlign: TextAlign.left),
          ),
          SizedBox(height: 10,),
          SimpleDialogOption(
            onPressed: () {Navigator.pushReplacementNamed(context, '/');}, //Navigator.pop(context, Department.treasury); },
            child: Text(S.of(context).lab1Text),
          ),
          SimpleDialogOption(
            onPressed: () {Navigator.pushReplacementNamed(context, '/');}, //Navigator.pop(context, Department.state); },
            child: Text(S.of(context).lab2Text),
          ),
        ],
      ),
    );
  }
}
