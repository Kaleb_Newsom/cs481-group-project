import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'generated/l10n.dart';

class homeScreen extends StatefulWidget {
  @override
  _homeScreenState createState() => _homeScreenState();
}

class _homeScreenState extends State<homeScreen> {
  @override
  Widget build(BuildContext context) {

    return MaterialApp(
      title: 'Internationalization, Localization and Dialogs',
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.blue[800],
          title: Center(
            child: Text(S.of(context).appBarTitle), //'Internationalization and Dialogs'
          ),
        ),
        body: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Center(
              child: SizedBox(
                width: 200,
                child: RaisedButton(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(18.0),
                  ),
                  child: Text(S.of(context).cupertinoDialogButtonName),
                  onPressed: () {
                    Navigator.pushNamed(context, '/cupertino');
                  },  //cupertinoDialog,
                  elevation: 3.0,
                  color: Colors.blue[200],
                ),
              ),
            ),
            Center(
              child: SizedBox(
                width: 200,
                child: RaisedButton(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(18.0),
                    ),
                    child: Text(S.of(context).alertDialogButtonName),
                    onPressed: () {
                      Navigator.pushNamed(context, '/alert');
                    },//=> alert,
                    elevation: 3.0,
                    color: Colors.blue[200]
                ),
              ),
            ),
            Center(
              child: SizedBox(
                width: 200,
                child: RaisedButton(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(18.0),
                    ),
                    child: Text(S.of(context).simpleDialogButtonName),
                    onPressed: () {
                      Navigator.pushNamed(context, '/simple');
                    },//=> simple,
                    elevation: 3.0,
                    color: Colors.blue[200]
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
