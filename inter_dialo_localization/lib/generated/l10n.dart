// GENERATED CODE - DO NOT MODIFY BY HAND
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'intl/messages_all.dart';

// **************************************************************************
// Generator: Flutter Intl IDE plugin
// Made by Localizely
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, lines_longer_than_80_chars

class S {
  S();
  
  static S current;
  
  static const AppLocalizationDelegate delegate =
    AppLocalizationDelegate();

  static Future<S> load(Locale locale) {
    final name = (locale.countryCode?.isEmpty ?? false) ? locale.languageCode : locale.toString();
    final localeName = Intl.canonicalizedLocale(name); 
    return initializeMessages(localeName).then((_) {
      Intl.defaultLocale = localeName;
      S.current = S();
      
      return S.current;
    });
  } 

  static S of(BuildContext context) {
    return Localizations.of<S>(context, S);
  }

  /// `High Traffic Notification`
  String get highTrafficTitle {
    return Intl.message(
      'High Traffic Notification',
      name: 'highTrafficTitle',
      desc: '',
      args: [],
    );
  }

  /// `High traffic in your area, expect traffic slowdowns. Plan accordingly.`
  String get highTrafficNotification {
    return Intl.message(
      'High traffic in your area, expect traffic slowdowns. Plan accordingly.',
      name: 'highTrafficNotification',
      desc: '',
      args: [],
    );
  }

  /// `Dismiss`
  String get highTrafficDismissButton {
    return Intl.message(
      'Dismiss',
      name: 'highTrafficDismissButton',
      desc: '',
      args: [],
    );
  }

  /// `Heat Advisory Warning!`
  String get highHeatTitle {
    return Intl.message(
      'Heat Advisory Warning!',
      name: 'highHeatTitle',
      desc: '',
      args: [],
    );
  }

  /// `High temperatures expected in San Diego for the next several days. Stay cool and well hydrated.`
  String get highHeatNotification {
    return Intl.message(
      'High temperatures expected in San Diego for the next several days. Stay cool and well hydrated.',
      name: 'highHeatNotification',
      desc: '',
      args: [],
    );
  }

  /// `Dismiss`
  String get highHeatDismissButton {
    return Intl.message(
      'Dismiss',
      name: 'highHeatDismissButton',
      desc: '',
      args: [],
    );
  }

  /// `Assignment Selection`
  String get assignmentSelectionText {
    return Intl.message(
      'Assignment Selection',
      name: 'assignmentSelectionText',
      desc: '',
      args: [],
    );
  }

  /// `Select the assignment you wish to view.`
  String get assignmentSelectViewText {
    return Intl.message(
      'Select the assignment you wish to view.',
      name: 'assignmentSelectViewText',
      desc: '',
      args: [],
    );
  }

  /// `Lab 1`
  String get lab1Text {
    return Intl.message(
      'Lab 1',
      name: 'lab1Text',
      desc: '',
      args: [],
    );
  }

  /// `Lab 2`
  String get lab2Text {
    return Intl.message(
      'Lab 2',
      name: 'lab2Text',
      desc: '',
      args: [],
    );
  }

  /// `Intl, Localilization, and Dialogs`
  String get appBarTitle {
    return Intl.message(
      'Intl, Localilization, and Dialogs',
      name: 'appBarTitle',
      desc: '',
      args: [],
    );
  }

  /// `Cupertino Dialog`
  String get cupertinoDialogButtonName {
    return Intl.message(
      'Cupertino Dialog',
      name: 'cupertinoDialogButtonName',
      desc: '',
      args: [],
    );
  }

  /// `Alert Dialog`
  String get alertDialogButtonName {
    return Intl.message(
      'Alert Dialog',
      name: 'alertDialogButtonName',
      desc: '',
      args: [],
    );
  }

  /// `Simple Dialog`
  String get simpleDialogButtonName {
    return Intl.message(
      'Simple Dialog',
      name: 'simpleDialogButtonName',
      desc: '',
      args: [],
    );
  }
}

class AppLocalizationDelegate extends LocalizationsDelegate<S> {
  const AppLocalizationDelegate();

  List<Locale> get supportedLocales {
    return const <Locale>[
      Locale.fromSubtags(languageCode: 'en', countryCode: 'US'),
      Locale.fromSubtags(languageCode: 'de'),
      Locale.fromSubtags(languageCode: 'en'),
      Locale.fromSubtags(languageCode: 'pt', countryCode: 'BR'),
    ];
  }

  @override
  bool isSupported(Locale locale) => _isSupported(locale);
  @override
  Future<S> load(Locale locale) => S.load(locale);
  @override
  bool shouldReload(AppLocalizationDelegate old) => false;

  bool _isSupported(Locale locale) {
    if (locale != null) {
      for (var supportedLocale in supportedLocales) {
        if (supportedLocale.languageCode == locale.languageCode) {
          return true;
        }
      }
    }
    return false;
  }
}